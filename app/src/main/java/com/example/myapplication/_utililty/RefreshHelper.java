package com.example.myapplication._utililty;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class RefreshHelper {
    public static void setSwipeRefresh(SwipeRefreshLayout swipeRefreshLayout, boolean refreshing){
         swipeRefreshLayout.setRefreshing(refreshing);
    }
}
