package com.example.myapplication.ui.post;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.post.PostResponse;

import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {

    private static final String TAG = "PostAdapter";
    private Context context;
    private List<PostResponse> list;
    private PostCallback listener;

    public PostAdapter(Context context, List<PostResponse> list, PostCallback listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final String title = list.get(position).getTitle();
        final String body = list.get(position).getBody();
        final int id = list.get(position).getId();

        holder.tvTitlePost.setText(title);
        holder.tvBodyPost.setText(body);

        holder.conlayItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClicked(position);
            }
        });

        settingGanjilGenap(holder, id);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void clearItem() {
        final int size = list.size();
        list.clear();
        notifyItemRangeRemoved(0, size);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitlePost;
        TextView tvBodyPost;
        ConstraintLayout conlayItem;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitlePost = itemView.findViewById(R.id.tvTitlePost);
            tvBodyPost = itemView.findViewById(R.id.tvBodyPost);
            conlayItem = itemView.findViewById(R.id.conlayItem);
        }
    }

    /**
     * logic UI untuk setiap item
     */
    private void settingGanjilGenap(ViewHolder holder, int id) {
        if (id % 2 == 0) {
            // action id bernilai genap
            holder.conlayItem.setBackgroundColor(context.getResources().getColor(android.R.color.holo_green_light));
        } else if (id % 2 != 0) {
            // action id bernilai ganjil
            holder.conlayItem.setBackgroundColor(context.getResources().getColor(android.R.color.background_light));

        }
    }


    /**
     * callback untuk dilempar ke view/UI/screen yg mengimplement interface ini
     */
    public interface PostCallback {
        void onItemClicked(int position); /* callback respon setelah klik 1 item dgn parameter posisi dari list data */
    }
}
