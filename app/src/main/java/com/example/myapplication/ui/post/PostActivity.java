package com.example.myapplication.ui.post;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.myapplication.R;
import com.example.myapplication.model.post.PostResponse;
import com.example.myapplication.presenter.post.PostInterface;
import com.example.myapplication.presenter.post.PostPresenter;

import java.util.List;

import static com.example.myapplication._utililty.RefreshHelper.setSwipeRefresh;

public class PostActivity extends AppCompatActivity
        implements PostInterface, PostAdapter.PostCallback {

    private static final String TAG = "PostAct"; // TAG utk fragment: "PostAct"
    private boolean isLoadingData; // status sedang loading data
    private PostPresenter presenter;
    private List<PostResponse> list = null;
    private PostAdapter adapter;

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private TextView tvEmptyPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        /* initialize view */
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        recyclerView = findViewById(R.id.recyclerView);
        tvEmptyPost = findViewById(R.id.tvEmpty);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        /* view/UI/screen (activity/fragment) men-deklarasi si presenter */
        presenter = new PostPresenter(this); // this, karena View/UI/screen telah implements PostInterface
        presenter.getPost(); /* 2.a. Presenter meng-implement method2 dari PostInterface */

        // event listener
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!isLoadingData) { /* action refresh request */
                    presenter.getPost();
                }
            }
        });
    }


    @Override
    public void onLoading() {
        isLoadingData = true;
        setSwipeRefresh(swipeRefreshLayout, true);
    }

    @Override
    public void onFinishLoad() {
        isLoadingData = false;
        setSwipeRefresh(swipeRefreshLayout, false);
    }

    @Override
    public void showData(List<PostResponse> response) {
        if (response.size()!=0) {
            showUI();
            list = response;
            setRecyclerView(list);
        }else {
            hideUI();
        }
    }

    private void setRecyclerView(List<PostResponse> response) {
        adapter = new PostAdapter(PostActivity.this, response, this);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onError(Throwable throwable) {
        Log.e(TAG, throwable.getMessage());
    }

    @Override
    public void showMessageError(String messageError) {
        Toast.makeText(this, messageError, Toast.LENGTH_LONG).show();
        hideUI();
    }

    private void hideUI() {
        tvEmptyPost.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    private void showUI() {
        tvEmptyPost.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        /* refresh request */
        presenter.getPost();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void onItemClicked(int position) {
        Toast.makeText(PostActivity.this, list.get(position).getTitle(), Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.clearItem();
    }

}
