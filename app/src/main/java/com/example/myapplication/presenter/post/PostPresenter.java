package com.example.myapplication.presenter.post;

import android.util.Log;

import com.example.myapplication._utililty.ApiService;
import com.example.myapplication.model.post.PostResponse;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostPresenter {

    private static final String TAG = "PostPresenter";
    private PostService apiService = ApiService.builderApi().create(PostService.class);
    private PostInterface listener;

    public PostPresenter(PostInterface postInterface) {
        this.listener = postInterface;
    }

    public void getPost() {
        listener.onLoading(); // override loading

        final Call<List<PostResponse>> call = apiService.getPostAll();
        call.enqueue(new Callback<List<PostResponse>>() {
            @Override
            public void onResponse(Call<List<PostResponse>> call, Response<List<PostResponse>> response) {

                cancelRequest(call);
                listener.onFinishLoad();

                switch (response.code()) {
                    case 200: /* action after response OK */
                        listener.showData(response.body());  // override response successfull
                        break;
                    default:
                        String messageError = "";
                        try {
                            /* get error body */
                            if (response.errorBody() != null)
                                listener.showMessageError(response.errorBody().string()); // override response show message error body
                        } catch (IOException e) {
                            Log.e(TAG, e.toString());
                        }
                        Log.e(TAG, messageError);
                        break;
                }
            }

            @Override
            public void onFailure(Call<List<PostResponse>> call, Throwable throwable) {
                cancelRequest(call);
                listener.onFinishLoad(); // override after loading finished
                listener.onError(throwable); // override response error
            }
        });
    }

    private void cancelRequest(Call<List<PostResponse>> call) {
        if (!call.isCanceled())
            call.cancel();
    }

    public void onDestroy() {
        listener = null;
    }
}
