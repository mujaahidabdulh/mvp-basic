package com.example.myapplication.presenter.post;

import com.example.myapplication.model.post.PostResponse;

import java.util.List;

public interface PostInterface {
    void onLoading();
    void onFinishLoad();
    void showData(List<PostResponse> response);
    void onError(Throwable t);
    void showMessageError(String messageError);
}
