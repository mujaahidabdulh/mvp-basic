package com.example.myapplication.presenter.post;

import com.example.myapplication.model.post.PostResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PostService {

    @GET("/posts")
    Call<List<PostResponse>> getPostAll();
}
